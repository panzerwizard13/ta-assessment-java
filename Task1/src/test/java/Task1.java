import com.epam.training.assessment.ilya_parakhin.LogInPage;
import org.ini4j.Profile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.ini4j.Ini;
import java.io.IOException;
import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.cssSelector;

public class Task1 {
    static Logger logger = LoggerFactory.getLogger(Task1.class);
    private WebDriver driver;
    String wrongPassword;
    String correctPassword;
    String wrongEmail;
    String correctEmail;

    static private void freezeThread(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void browserSetup() {
        try {
            Ini ini = new Ini(new File("src\\main\\resources\\properties.ini"));
            Profile.Section passwordSection = ini.get("passwordSection");
            wrongPassword = passwordSection.get("wrongPassword");
            correctPassword = passwordSection.get("correctPassword");
            Profile.Section emailSection = ini.get("emailSection");
            wrongEmail = emailSection.get("wrongEmail");
            correctEmail = emailSection.get("correctEmail");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get("https://open.spotify.com/");
        WebElement element1 = driver.findElement(cssSelector(".Button-sc-qlcn5g-0.KQWHP"));
        element1.click();

    }

    @Test
    public void emptyCredentials() {
        logger.info("emptyCredentials");
        LogInPage logpage = new LogInPage(driver);
        logpage.logIn("asdsd", "");
        logger.error("Error message is: " + driver.findElement(By.cssSelector(".Message-sc-15vkh7g-0.dHbxKh")).getText());
        //guess it's better to make a string of expected result and compare error message to it, but since the website
        //and error messages could be in different languages, depending on region I implemented silly check;
        freezeThread(2000);
        logger.error("Error message is: " + driver.findElement(By.cssSelector(".Message-sc-15vkh7g-0.dHbxKh")).getText());
        logpage.clearLogIn();
        logpage.logIn("", "");
        if (driver.getTitle().equals("Spotify - Web Player: Music for everyone")) Assert.fail();
        else Assert.assertTrue(true);
    }

    @Test
    public void invalidCredentials() {
        logger.info("invalidCredentials");
        LogInPage logpage = new LogInPage(driver);
        logpage.logIn(wrongEmail, wrongPassword);
        freezeThread(2000);
        logger.error("Error message is: " + driver.findElement(By.cssSelector(".Message-sc-15vkh7g-0.dHbxKh")).getText());
        if (driver.getTitle().equals("Spotify - Web Player: Music for everyone")) Assert.assertTrue(true);
    }

    @Test
    public void correctCredentials() {
        logger.info("correctCredentials");
        LogInPage logpage = new LogInPage(driver);
        logpage.logIn(correctEmail, correctPassword);
        freezeThread(2000);
        if (driver.getTitle().equals("Spotify - Web Player: Music for everyone")) Assert.assertTrue(true);
    }

    @AfterMethod(alwaysRun = true)
    public void browserShutDown() {
        driver.quit();
        driver = null;
    }
}
