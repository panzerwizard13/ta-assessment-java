package com.epam.training.assessment.ilya_parakhin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LogInPage {
    protected WebDriver driver;

    public LogInPage(WebDriver driver) {
        if (driver == null) throw new IllegalArgumentException();
        this.driver = driver;
    }

    public void logIn(String email, String password) {
        new WebDriverWait(driver, Duration.ofSeconds(3)).until(ExpectedConditions.elementToBeClickable(By.id("login-button")));
        driver.findElement(By.id("login-username")).sendKeys(email);
        driver.findElement(By.id("login-password")).sendKeys(password);
        driver.findElement(By.id("login-button")).click();

    }

    public void clearLogIn() {
        driver.findElement(By.id("login-username")).clear();
        driver.findElement(By.id("login-password")).clear();
    }
}
